<?php

namespace app\assets;

use yii\web\AssetBundle;


class AEIFrontAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/aei/assets';
    
    public $css = [
        'https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&amp;subset=cyrillic,latin-ext',
        'css/uikit.min.css',
        'css/uikit.almost-flat.min.css',
        'css/animate.css',
        'css/magnific-popup.css',
        'css/font-awesome.min.css',
        'css/thumbnail-slider.css',
        'css/ninja-slider.css',
        'css/main.css',
    ];
       
    public $js = [
        'js/jquery.js',
        'js/uikit.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/jquery.matchHeight.js',
        'js/wow.min.js',
        //'https://maps.googleapis.com/maps/api/js?key=AIzaSyCzj1nfe88iJps7q6Zbw6NsY1qeUtGhzxY&callback=initMap',
        'js/thumbnail-slider.js',
        'js/ninja-slider.js',
        'js/scripts.js',
    ];
     
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\web\JqueryAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'rmrevin\yii\fontawesome\AssetBundle'
    ];
    
}