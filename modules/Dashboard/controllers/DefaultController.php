<?php

namespace app\modules\Dashboard\controllers;

use app\controllers\BackendController;
use Yii;

/**
 * Default controller for the `Dashboard` module
 */
class DefaultController extends BackendController
{
    
    public function init()
    {
        parent::init();
    }
    
    public function permissions()
    {
        return [
            'index' => [
                'permission' => 'view_dashboard',
                'label' => Yii::t("app", 'Access to dashboard')
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
