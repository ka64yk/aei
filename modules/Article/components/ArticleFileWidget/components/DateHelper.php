<?php

namespace app\modules\Article\components\ArticleFileWidget\components;

use Yii;

class DateHelper
{
    
    public static function monthsList()
    {
        return [
            '' => '-',
            1 => Yii::t('date', 'January'),
            2 => Yii::t('date', 'February'),
            3 => Yii::t('date', 'March'),
            4 => Yii::t('date', 'April'),
            5 => Yii::t('date', 'May'),
            6 => Yii::t('date', 'June'),
            7 => Yii::t('date', 'July'),
            8 => Yii::t('date', 'August'),
            9 => Yii::t('date', 'September'),
            10 => Yii::t('date', 'October'),
            11 => Yii::t('date', 'November'),
            12 => Yii::t('date', 'December'),
        ];
    }
    
}
