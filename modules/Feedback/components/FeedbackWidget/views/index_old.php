<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;

?>

<div class="form">

<?php Pjax::begin(['options' => ['tag'=>false]]) ?>
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>
        <?= Html::tag('div', Yii::t('app', 'Feedback send'), [
            'class' => 'alert alert-success'
        ]) ?>
    <?php } else { ?>
    
        <?php $form = ActiveForm::begin([
            'id' => 'feedback-form',
            'options' => [
                'data-pjax' => ''
            ]
        ]); ?>

            <?= $form->field($model, 'Name')->textInput() ?>

            <?= $form->field($model, 'Phone')->textInput() ?>

            <?= $form->field($model, 'Email')->textInput() ?>

            <?= $form->field($model, 'Message')->textarea([
                'style' => 'min-height:100px;'
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> 
            </div>

        <?php ActiveForm::end(); ?>
    
    <?php } ?>
    
<?php Pjax::end() ?>

</div>