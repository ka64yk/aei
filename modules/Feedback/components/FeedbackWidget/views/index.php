<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;

?>

<div class="form">

<?php Pjax::begin(['options' => ['tag'=>false]]) ?>
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>
        <?= Html::tag('div', Yii::t('app', 'Feedback sent'), [
            'class' => 'alert alert-success'
        ]) ?>
    <?php } else { ?>
    
        <?php $form = ActiveForm::begin([
            'id' => 'feedback-form',
            'options' => [
                'data-pjax' => ''
            ]
        ]); ?>

            <label>
                <input type="text" name="Feedback[Name]" placeholder="Nume/Prenume" required="required">
            </label>
            <label>
                <input type="text" name="Feedback[Phone]" placeholder="Telefon" required="required">
            </label>
            <label>
                <input type="text" name="Feedback[Email]" placeholder="Email" required="required">
            </label>
            <label>
                <textarea name="Feedback[Message]" placeholder="Mesaj" required="required"></textarea>
            </label>
            <button type="submit">
                Trimite <i class="uk-icon-send"></i>
            </button>

        <?php ActiveForm::end(); ?>
    
    <?php } ?>
    
<?php Pjax::end() ?>

</div>