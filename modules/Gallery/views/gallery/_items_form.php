<?php

    use yii\helpers\Html;
    use kartik\sortable\Sortable;
    use yii\widgets\Pjax;
    use yii\helpers\Url;

?>
<br />

<?= $this->render('upload-images-form', [
    'model' => $model
]) ?>

<?php Pjax::begin([
    'id' => 'gallery-item-list'
]) ?>
    <?php $items = []; ?>
    <?php foreach ($model->items as $item) {
        $items[]['content'] = "
            <div style=\"position:relative;\" class=\"gallery-item\" data-id=\"" . $item->ID . "\">
                <div class=\"gallery-item-image\">
                    " . Html::img('@web/uploads/gallery/' . $item->Thumb, [
                        'width' => 200,
                        'height' => 200
                    ]) . "
                </div>
                <div style=\"position:absolute; left:0; bottom:0; width:100%; background:#fff;\" class=\"gallery-item-options text-center\">
                    <a class=\"delete-gallery-item\" data-id=\"" . $item->ID . "\" style=\"cursor:pointer; font-size: 20px;\"><i class=\"fa fa-trash text-danger\"></i></a>
                </div>
            </div>
        ";
    } ?>

    <?= Sortable::widget([
        'type'=>'grid',
        'items'=> $items,
        'itemOptions' => [
            'width' => '200',
            'height' => '200',
        ]
    ]); ?>

    <?php $this->registerJs('
        $(".sortable.grid").on("sortupdate", function(e) {
            var ids = [];
            $(".sortable.grid > li .gallery-item").each(function(){
                ids.push($(this).attr("data-id"));
            });
            $.post("' . Url::to(['/admin/gallery/gallery/sort-images']) . '", {ids: ids});
        });
    ') ?>

<?php Pjax::end() ?>

<?php $this->registerCss("
    .gallery-form .sortable-placeholder {
        width: 218px;
        height: 218px;
    }
") ?>

<?php $this->registerJs('
    $(document).on("click", ".delete-gallery-item", function(){
        $.post("' . Url::to(['/admin/gallery/gallery/delete-image']) . '", {id: $(this).attr("data-id")}, function(){
            $.pjax.reload({container: "#gallery-item-list"});
        });
    });
') ?>