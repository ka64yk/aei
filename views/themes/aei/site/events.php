<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\assets\AEIFrontAssets;
use app\modules\Settings\Settings;

$this->title = $article->lang->Title;

$bundle = AEIFrontAssets::register($this);

$query = Article::getDb()->cache(function ($db) {
    return Article::find()->where(['Type' => 'Event'])->with(['lang'])->orderBy('Date DESC');
}, 60);

$articlesPerPage = Settings::getByName('articlesPerPage');

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    
    'pagination' => [
        'pageSize' => $articlesPerPage,
        
    ],
]);

?>

<div class='uk-grid'>
    
    <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'event_list_item',
            'layout' => "{items}\n<div class='uk-width-1-1 uk-clearfix'><div class='pagination'>{pager}</div></div>",
            'viewParams' => ['bundle' => $bundle],
            'options' => ['tag'=>false],
            'itemOptions' => ['tag'=>false],
            'pager' => [
                'prevPageLabel' => '<span class="uk-icon-chevron-left"></span>',
                'nextPageLabel'  => '<span class="uk-icon-chevron-right"></span>',                                
            ],
            
        ]); ?>
    
    <?php Pjax::end(); ?>

</div>