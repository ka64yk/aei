<?php

    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    
    $this->title = $article->lang->Title;
?>


   
    <div>
        <?= $article->lang->Text ?>
    </div>
<div class="uk-grid">
    <?= ArticleImageWidget::widget([
        'article' => $article,
        'view' => '/site/partners_image',
    ]) ?>
</div>
