<?php use yii\helpers\Html; ?>
<div class="posts-box1 post-box uk-clearfix">
    <a href="<?= $event->seoLink ?>">
        <?= Html::img( !empty($event->MainThumbUrl) ? $event->MainThumbUrl : $bundle->baseUrl . '/images/post-box-img.png') ?>
    </a>
    <span>
        <?= $event->dMYDate ?>
    </span>
    <h5>
        <a href="<?= $event->seoLink ?>">
            <?= $event->lang->Title ?>
        </a>
    </h5>
    <p>
        <?= $event->getShortText(200) ?>
    </p>
</div>