<?php

    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;    
    use yii\helpers\Html;
    use app\assets\AEIFrontAssets;
    
    $bundle = AEIFrontAssets::register($this);
    
    $this->title = $article->lang->Title;
    
?>
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <?= Html::img($bundle->baseUrl . '/images/creditare-img.png') ?>
                            <div class="paragraph mt20">
                                <?= $article->lang->Text ?>
                            </div>                            
                            <div class="document-section">
                                <h4>
                                    <?= Yii::t("app", "Attachments") ?>
                                </h4>
                                <div class="uk-grid">
                                    
                                     <?= ArticleFileWidget::widget([
                                        'article' => $article,
                                        'view' => '/site/event_list_item_file',
                                    ]) ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

