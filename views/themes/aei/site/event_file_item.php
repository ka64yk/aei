<?php

use yii\helpers\Html;
use app\assets\AEIFrontAssets;
$bundle = AEIFrontAssets::register($this); 
    
?>

<div class="uk-width-medium-1-3">
    <div class="document-box">
        <div class="table">
            <div class="middle left-text">
                <?= Html::a(
                    !empty($model->lang->Title) ? $model->lang->Title : $model->File,
                    $model->fullLink, 
                    [
                        'title' => $model->lang->Title,
                        'target' => '_blank',
                    ]) 
                ?>
            </div>
            <div class="middle right-text">                
                <?= Html::a(
                    Html::img($bundle->baseUrl . "/images/pdf-(1).png"),
                    $model->fullLink, 
                    [
                        'title' => $model->lang->Title,
                        'target' => '_blank',
                    ]) 
                ?>
            </div>
        </div>
    </div>
</div>

    
