<?php

    use yii\helpers\Html;
    use bl\files\icons\FileIconWidget;
    
?>

<?php $iconWidget = FileIconWidget::begin([
    'useDefaultIcons' => true
]); ?>
<li>
    <?= $model->lang->Title ?>
    <?= Html::a(
        $iconWidget->getIcon($model->File) . ' ' . "PDF" ,
        $model->fullLink, 
        [
            'title' => $model->lang->Title,
            'style' => '',
            'target' => '_blank',
            'data-pjax' => 0,
            'class' => ''
        ]) 
    ?>
</li>

<?php FileIconWidget::end() ?>