<?php

    use yii\widgets\ActiveForm;
    use app\modules\Article\components\ArticleFileWidget\components\DateHelper;
    use yii\widgets\ListView;
    use yii\widgets\Pjax;
    use yii\helpers\Html;
    

?>


<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'file_item',
    'layout' => "<ul>{items}</ul>",
    'options' => [
        'class' => 'list-group'
    ]
]); ?>
    
