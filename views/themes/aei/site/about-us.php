<?php

use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use app\models\Article\Article;

$this->title = $article->lang->Title;

$aboutUs = Article::find()->where(["ID" => 85])->with(["childs"])->one();
?>

<?php foreach($aboutUs->childs as $ac) { ?>

    <h4><?= $ac->childArticle->lang->Title ?></h4>
    <?= !empty($ac->childArticle->lang->Text) ? "<p>" . $ac->childArticle->lang->Text . "</p>" : "" ?>
    <ul>
    <?= ArticleFileWidget::widget([
        "article" => $ac->childArticle, 
        "view" => "@app/views/themes/aei/site/pdf_files",                                                
        ]) ?>
    </ul>
    
<?php } ?>  
