<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Gallery\Gallery;
use app\models\GalleryItem\GalleryItem;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use app\modules\Settings\Settings;
use app\assets\AEIFrontAssets;

$this->title = $article->lang->Title;

$articlesPerPage = Settings::getByName('articlesPerPage');

$bundle = AEIFrontAssets::register($this);

$query = Gallery::getDb()->cache(function ($db) {
    return Gallery::find()->with(['lang', 'items'])->orderBy('Position');
}, 60);

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => $articlesPerPage,
    ],
]);

?>

    
    <div class="uk-grid">
        
        <?php //Pjax::begin(['options' => ['tag'=>false]]); ?>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'gallery_list_item',
                'layout' => "{items}\n<div class='uk-width-1-1 uk-clearfix'><div class='pagination'>{pager}</div></div>",
                'viewParams' => ['bundle' => $bundle],
                'options' => ['tag'=>false],
                'itemOptions' => ['tag'=>false],
                
            ]); ?>
        <?php //Pjax::end(); ?>
        
    </div>



