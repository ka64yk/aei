<?php

    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    
    $this->title = $article->lang->Title;
    
?>
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <?= yii\helpers\Html::img($article->MainImageUrl) ?>
                            <div class="paragraph mt20">
                                <?= $article->lang->Text ?>
                            </div>
                            <div class="galery">
                                <h4>
                                    <?= Yii::t("app", "Gallery") ?>
                                </h4>
                                <div class="popup-gallery">
                                    <div class="uk-grid">
                                        
                                    <?= ArticleImageWidget::widget([
                                        'article' => $article,
                                        'view' => '/site/event_list_item_image',
                                    ]) ?>
                                        
                                    </div>
                                </div>                                                        
                            </div>
                            <div class="document-section">
                                <h4>
                                    <?= Yii::t("app", "Attachments") ?>
                                </h4>
                                <div class="uk-grid">
                                    
                                     <?= ArticleFileWidget::widget([
                                        'article' => $article,
                                        'view' => '/site/event_list_item_file',
                                    ]) ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>