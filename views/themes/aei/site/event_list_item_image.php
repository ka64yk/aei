<?php foreach($article->images as $img){ ?>
<div class="uk-width-1-3">
    <div class="img-box">
        <a href="/uploads/article/<?= $img['Image'] ?>">
            <?= yii\helpers\Html::img("/uploads/article/" . $img['Thumb']) ?>
            <i class="uk-icon-eye"></i>
        </a>
    </div>
</div>
<?php } ?>