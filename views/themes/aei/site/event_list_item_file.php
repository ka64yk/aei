<?php

    use yii\widgets\ActiveForm;
    use app\modules\Article\components\ArticleFileWidget\components\DateHelper;
    use yii\widgets\ListView;
    use yii\widgets\Pjax;
    use yii\helpers\Html;
    

?>


<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'event_file_item',
    'layout' => "{items}",
    'options' => ['tag'=>false],
    'itemOptions' => ['tag'=>false],
]); ?>


