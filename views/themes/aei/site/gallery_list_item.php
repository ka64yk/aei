<div class="uk-width-medium-1-3" data-pjax="0">
    <div class="evenimente-box item4">
        <div class="img-box">
            <a href="#my_modal<?= $model->ID ?>" class="modal">
                <?= yii\helpers\Html::img('@web/uploads/gallery/' . $model->Thumb) ?>
            </a>
        </div>
        <div class="title-box item5 uk-text-center">
            <a href="#my_modal<?= $model->ID ?>" class="modal">
                <?= $model->lang->Title ?>
            </a>
        </div>
    </div>
</div>


    <div id="my_modal<?= $model->ID ?>" class="gallery mfp-hide uk-clearfix">
        <div class="title">
            <h4>
                <?= $model->lang->Title ?>
            </h4>
        </div>
        <div class="uk-grid pop-up-slider">
            <div class="uk-width-medium-7-10">
                <div class="slider">
                    <?php $i=1; foreach (\app\models\GalleryItem\GalleryItem::find()->where(['GalleryID' => $model->ID])->orderBy('Position')->all() as $image) { ?>
                    
                        <?= 
                            yii\helpers\Html::img(
                                '@web/uploads/gallery/' . $image->Value, 
                                [
                                    'class' => 'mySlides', 
                                    'style' => 'display:'.($i == 1 ? "block" : "none"),
                                ]
                            ); 
                        ?>
                    
                    <?php $i++; } ?>                    
                    <button class="prev" onclick="plusDivs('my_modal<?= $model->ID ?>', -1)">
                        <?= yii\helpers\Html::img( $bundle->baseUrl . '/images/left.png'); ?>
                    </button>
                    <button class="next" onclick="plusDivs('my_modal<?= $model->ID ?>', +1)">
                        <?= yii\helpers\Html::img( $bundle->baseUrl . '/images/right.png'); ?>
                    </button>
                </div>
            </div>
            <div class="uk-width-medium-3-10">
                <div class="prev-box">
                    <a class="prev uk-text-center">
                        <i class="uk-icon-chevron-up"></i>
                    </a>
                </div>
                <div class="scrool-box">
                    <div class="uk-grid">
                        
                        <?php $i=1; foreach (\app\models\GalleryItem\GalleryItem::find()->where(['GalleryID' => $model->ID])->orderBy('Position')->all() as $image) { ?>
                        <div class="uk-width-1-2">
                            <div class="dots-box">
                                <a href="#">
                                    
                            <?= 
                                yii\helpers\Html::img(
                                    '@web/uploads/gallery/' . $image->Thumb, 
                                    [
                                        'class' => 'dot '.($i == 1 ? "active" : ""),
                                        'onclick' => 'currentDiv("my_modal'. $model->ID .'", '.$i.')',
                                    ]
                                ); 
                            ?>
                                    <i class="uk-icon-eye"></i>
                                </a>
                            </div>
                        </div>

                        <?php $i++; } ?>                        
                        
                    </div>
                </div>
                <div class="next-box">
                    <a class="next uk-text-center">
                        <i class="uk-icon-chevron-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <p><a class="mfp-close" href="#">X</a></p>
    </div>