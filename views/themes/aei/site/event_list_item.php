<div class="uk-width-medium-1-3" data-pjax="0">
    <div class="evenimente-box item4">
        <div class="img-box">
            <a href="<?= $model->seoLink ?>">
                <?= yii\helpers\Html::img(!empty($model->MainThumbUrl) ? $model->MainThumbUrl : yii\helpers\Url::to( $bundle->baseUrl . '/images/post-box-img.png') ) ?>
            </a>
        </div>
        <div class="title-box item5 uk-text-center">
            <a href="<?= $model->seoLink ?>">
                <?= $model->lang->Title ?>
            </a>
        </div>
        <div class="description-box item6 uk-text-center">
            <p>
                <?= $model->shortText ?>
            </p>
        </div>
    </div>
</div>