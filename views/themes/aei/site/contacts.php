<?php

    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\modules\Settings\Settings;
    
    $this->title = $article->lang->Title;
?>

<div class="container">
    
    <div class="row">
        <div class="col-md-6">
            <?= $article->lang->Text ?>
            <hr />
            <?= FeedbackWidget::widget() ?>
            <hr />
            <?= Settings::getByName('address', true) ?>
            <hr />
            <?= Settings::getByName('grafic', true) ?>
        </div>
        <div class="col-md-6">
            <?= Settings::getByName('map') ?>
        </div>
    </div>
    
    <?= ArticleImageWidget::widget([
        'article' => $article
    ]) ?>
    
</div>