<?php

use yii\bootstrap\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\assets\AEIFrontAssets;

$this->title = $article->lang->Title;

$bundle = AEIFrontAssets::register($this);

$query = Article::getDb()->cache(function ($db) {
    return Article::find()->where(['Type' => 'Services'])->with(['lang'])->orderBy('Date DESC');
}, 60);

$dataProvider = new ActiveDataProvider([
    'query' => $query,
]);

?>

   
<div class="tab-box servicii-page">
   <div class="uk-grid">
       <div class="uk-width-1-1">
           <div class="img-box">
               <?= Html::img($bundle->baseUrl . '/images/verne-ho-26448-copy-1.png') ?>
               <div class="paragraph uk-text-center">

                   <p><?= $article->lang->Text ?></p>

               </div>
           </div>
       </div>
       <div class="uk-width-medium-4-10 padding">
           <div class="container item7">
               <a class="drop-down" href="#">
                   <?= Yii::t("app", "OurServices") ?>
                   <i class="uk-icon-navicon"></i>
               </a>
               <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tab'}">

                   <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'services_list_item_menu',
                            'layout' => "{items}",
                            'options' => ['tag'=>false],
                            'itemOptions' => ['tag'=>false],
                        ]); ?>
                    <?php Pjax::end(); ?>

               </ul>
           </div>
       </div>
       <div class="uk-width-medium-6-10">
           <div class="tab-content item7">
               <ul id="tab" class="uk-switcher">                                      

                    <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'services_list_item_content',
                            'layout' => "{items}",
                            'options' => ['tag'=>false],
                            'itemOptions' => ['tag'=>false],
                        ]); ?>
                    <?php Pjax::end(); ?>                                           

               </ul>
           </div>
       </div>
   </div>
</div>