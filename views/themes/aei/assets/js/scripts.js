


/* GOOGLE MAPS WITH MARKER */
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: {lat: 47.0321659, lng: 28.804726},
        zoomControl: false,
        disableDoubleClickZoom: true,
        scrollwheel: false,
        disableDefaultUI: true,
        styles: [
            {
                "featureType": "administrative.country",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#014a7b"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels",
                "stylers": [
                    {
                        "color": "#014a7b"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#01548c"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#01548c"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#b5e1ff"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#d9efff"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#d9efff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d9efff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#01548c"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#b5e1ff"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#01548c"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "color": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#014a7b"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "color": "#1580c4"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#94d4ff"
                    }
                ]
            }
        ]
    });
    var image = THEME_URL + '/images/placeholder.png';
    var beachMarker = new google.maps.Marker({
        position: {lat: 47.0321659, lng: 28.804726},
        map: map,
        icon: image
    });
}

$(document).ready(function(){
    $('.item').matchHeight();
    $('.item2').matchHeight();
    $('.item3').matchHeight();
    $('.item4').matchHeight();
    $('.item5').matchHeight();
    $('.item6').matchHeight();
    $('.item7').matchHeight();
    $('.item8').matchHeight();



    var scrolled = 0;

    $(".next").on("click" ,function(){
        scrolled=$(".scrool-box").scrollTop();
        $(".scrool-box").animate({
            scrollTop:  scrolled + 120
        });
    });

    $(".prev").on("click" ,function(){
        scrolled=$(".scrool-box").scrollTop();
        $(".scrool-box").animate({
            scrollTop:  scrolled - 120
        });
    });



    $('.drop-down').on("click", function(e){
        e.preventDefault();
        $('.uk-tab-left').slideToggle();
    });


    $('.modal').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });
    $(document).on('click', '.mfp-close', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            title: false,
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '';
                }
            }
        });
        
        
    

});


var slideIndex = 1;
//showDivs(slideIndex);

function plusDivs(id, n) {
    showDivs(id, slideIndex += n);
}

function currentDiv(id, n) {
    showDivs(id, slideIndex = n);
}

function showDivs(id, n) {
    
    var x = $("#"+id+" .mySlides");
    var dots = $("#"+id+" .dot");
            
            
    var i;
//    var x = document.getElementsByClassName("mySlides");
//    var dots = document.getElementsByClassName("dot");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace("active", "");
    }
    x[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += "active";
   // $(".scrool-box").scrollTop($(".scrool-box .dot.active").offset().top - 120);
    console.log($(".scrool-box .dot.active").closest(".uk-width-1-2").offset().top - 120);
}




