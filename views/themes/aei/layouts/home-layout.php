<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\AEIFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use app\models\Article\Article;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;

$bundle = AEIFrontAssets::register($this);

$lastEvents = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => ['Event']])->orderBy('Date DESC')->limit(4)->offset(1)->all();
    }, 30);

$lastEvent = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => ['Event']])->orderBy('Date DESC')->limit(1)->one();
    }, 30);
    
$aboutUs = Article::find()->where(["ID" => 85])->with(["childs"])->one();

$partners = Article::find()->where(["ID" => 88])->with(["images"])->one();

$service_page = Article::find()->with(['lang', 'link'])->where(['ID' => 86])->one();

$servicesArticles = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => ['Services']])->orderBy('Date DESC')->limit(10)->all();
    }, 30);

    
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>', THEME_URL = '<?= $bundle->baseUrl ?>';</script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>    
 
        <div class="wrapper">
            <div class="background-header">
                <div class="uk-clearfix header">
                    <div class="uk-container-center uk-container">
                        <div class="uk-grid">
                            <div class="uk-width-9-10">
                                <div class="mobile-menu">
                                    <a href="#mobile_menu" data-uk-offcanvas>
                                        <i class="uk-icon-navicon"></i>
                                    </a>
                                </div>
                                <div class="top-menu">
                                    
                                    <?= MenuWidget::widget([
                                        'menuId' => 3,
                                    ]) ?> 
                                    
                                </div>
                            </div>
                            <div class="uk-width-1-10">
                                <div class="language-box">
                                    <?php foreach (Yii::$app->params['siteLanguages'] as $lang) { ?>
                                    <?php if(Yii::$app->language == $lang) continue; ?>
                                    <a href="<?= Url::current(['language' => $lang]) ?>">
                                        <?= $lang ?>
                                    </a>
                                    <?php } ?>  
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid uk-margin-top-remove">
                            <div class="uk-width-1-1">
                                <div class="table">
                                    <div class="middle">
                                        <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                                    </div>
                                    <div class="middle">
                                        <h1>
                                            <?= Yii::t("app", "AsociatiaCentralaEconomii") ?>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid mb60 bottom-menu">
                            <div class="uk-width-1-2">
                                <div class="uk-grid left">
                                    <div class="uk-width-medium-1-3">
                                        <div class="link-page">
                                            <a href="#">
                                                <i class="uk-icon-envelope"></i>
                                                <?= Yii::t("app", "Email") ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="link-page">
                                            <a href="#">
                                                <i class="uk-icon-sign-in"></i>
                                                <?= Yii::t("app", "ClientOnline") ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="link-page">
                                            <a href="#">
                                                <i class="fa fa-users"></i>
                                                <?= Yii::t("app", "Forum") ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-grid right">
                                    <div class="uk-width-medium-1-3">
                                        <div class="link-page">
                                            <a href="http://capital.market.md" target="_blank" class="padding1">
                                                <?= Html::img($bundle->baseUrl . '/images/capital.png') ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="link-page">
                                            <a href="#">
                                                <?= Html::img($bundle->baseUrl . '/images/finans-corporation.png') ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="link-page">
                                            <a href="#" class="margin1">
                                                <i><?= Html::img($bundle->baseUrl . '/images/sigla.png') ?></i>
                                                <?= Yii::t("app", "C.N.P.F.") ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-clearfix our-section">
                <div class="background">
                    <div class="uk-container-center uk-container">
                        <div class="uk-grid">
                            <div class="uk-width-medium-6-10">
                                <div class="title-section box1 uk-clearfix">
                                    <h3>
                                        <?= Yii::t("app", "LatestEvents") ?>
                                    </h3>
                                </div>
                                <div class="more-info-left-box uk-clearfix">
                                    <div class="info-box uk-clearfix item">
                                        
                                        <?php if (count($lastEvents) > 0) { ?>
                                        
                                        <div class="top-description">
                                            <div class="title-posts uk-clearfix">
                                                <p>
                                                    <a href="<?= $lastEvent->seoLink ?>">
                                                        <?= $lastEvent->lang->Title ?>                                                        
                                                    </a>
                                                </p>
                                            <span>
                                                <?= $lastEvent->dMYDate ?>
                                            </span>
                                            </div>
                                            <div class="description">
                                                <div class="table">
                                                    <div class="middle middle1">
                                                        <a href="<?= $lastEvent->seoLink ?>">
                                                            <?= Html::img(!empty($lastEvent->MainThumbUrl) ? $lastEvent->MainThumbUrl : $bundle->baseUrl . '/images/post-box-img.png') ?>
                                                        </a>
                                                    </div>
                                                    <div class="middle middle2">
                                                        <p>
                                                            <?= $lastEvent->getShortText(500) ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="posts uk-clearfix">                                            
                                        
                                            <?php foreach ($lastEvents as $event) { ?>
                                            <?= $this->render('../site/event_item', [
                                                'event' => $event,
                                                'bundle' => $bundle
                                            ]) ?>
                                            
                                            <?php } ?>
                                            <div class="more-results uk-text-center">
                                                <a href="<?= Url::to(["/evenimente"]) ?>">
                                                    <?= Yii::t("app", "ViewMore") ?>
                                                    <span> </span>
                                                </a>
                                            </div>
                                        </div>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-4-10">
                                <div class="title-section box2">
                                    <h3>
                                        <?= Yii::t("app", "AboutUsTitle") ?>
                                    </h3>
                                </div>
                                <div class="more-info-right-box uk-clearfix">
                                    <div class="info-box uk-clearfix item">
                                        <?php foreach($aboutUs->childs as $ac) { ?>
                                            <h4><?= $ac->childArticle->lang->Title ?></h4>
                                            <?= !empty($ac->childArticle->lang->Text) ? "<p>" . $ac->childArticle->lang->Text . "</p>" : "" ?>
                                            <ul>
                                            <?= ArticleFileWidget::widget([
                                                "article" => $ac->childArticle, 
                                                "view" => "@app/views/themes/aei/site/pdf_files",                                                
                                                ]) ?>
                                            </ul>
                                        <?php } ?>  
                                            
                                        
                                        <div class="more-results uk-text-center">
                                            <a href="<?= Url::to(["/about-us"]) ?>">
                                                <?= Yii::t("app", "ViewMore") ?>
                                                <span> </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-clearfix our-services-description">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="title-section uk-text-center">
                            <h3>
                                <?= Yii::t("app", "OurServiceTitle") ?>
                            </h3>
                        </div>
                        <div class="description uk-text-center">
                            <p>
                                <?= $service_page->lang->Text ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-clearfix our-services-description-tab">
                <div class="uk-grid">
                    <div class="uk-width-medium-4-10">
                        <div class="container item2">
                            <a class="drop-down" href="#">
                                <?= Yii::t("app", "OurServices") ?>
                                <i class="uk-icon-navicon"></i>
                            </a>
                            <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tab'}">
                                <?php foreach($servicesArticles as $sa) { ?>
                                <li class="uk-width-1-1">
                                    <a>
                                        <?= $sa->lang->Title ?>
                                        <i class="uk-icon-chevron-right"></i>
                                    </a>
                                </li>                                    
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="uk-width-medium-6-10">
                        <div class="tab-content item2">
                            <ul id="tab" class="uk-switcher">
                                
                                <?php foreach($servicesArticles as $sa) { ?>
                                <li class="uk-animation-slide-right">                                    
                                    <?= $sa->lang->Text ?>                                        
                                </li>                                    
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-clearfix parteners-section">
                <div class="uk-container-center uk-container">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="title-section uk-text-center">
                                <h3>
                                    <?= Yii::t("app", "PartnersTitle") ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid mt1">
                        <?php foreach($partners->images as $pi) { ?>
                        
                        <div class="uk-width-2-10">
                            <div class="parteners-box item3">
                                <a target="_blank" href="<?= $pi->lang->Title ?>">
                                    <?= Html::img($pi->imageLink) ?>
                                </a>
                            </div>
                        </div>                          
                            
                        <?php } ?>                         
                    </div>
                </div>
            </div>
            <div class="uk-clearfix our-adress">
                <div class="uk-grid">
                    <div class="uk-width-medium-6-10">
                        <div class="title-section box1 uk-clearfix">
                            <h3>
                                <?= Yii::t("app", "ContactTitle") ?>
                            </h3>
                        </div>
                        <div id="map" class="item3 noMatch">

                        </div>
                    </div>
                    <div class="uk-width-medium-4-10">
                        <div class="title-section box2">
                            <h3>
                                <?= Yii::t("app", "OurContactsTitle") ?>
                            </h3>
                        </div>
                        <div class="contacts">
                            <div class="contacts-box item3 noMatch">
                                <h4>
                                    <?= Yii::t("app", "Address") ?>
                                </h4>
                                <ul>
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/location.png') ?></i>
                                        <?= Settings::getByName('address', true) ?>
                                    </li>
                                    <li class="padding">
                                        <?= Settings::getByName('address2', true) ?>
                                    </li>
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/phone.png') ?></i>
                                        Tel/fax: <?= Settings::getByName('phone') ?>
                                    </li>
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/mail.png') ?></i>
                                        Email: <?= Settings::getByName('email') ?>
                                    </li>
                                </ul>
                                <h4>
                                    <?= Yii::t("app", "BankInfo") ?>
                                </h4>
                                <ul class="all-padding">
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/bank.png') ?></i>
                                        <?= Settings::getByName('bank_rec', true) ?>
                                    </li>
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont1', true) ?>
                                    </li>                                    
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont2', true) ?>
                                    </li>                                    
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont3', true) ?>
                                    </li>
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont4', true) ?>
                                    </li>
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont5', true) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-clearfix footer">
                <div class="uk-container-center uk-container">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="copyright uk-text-center">
                                <p>
                                    Copyright &copy; <?= date("Y") ?> <?= Yii::t("app", "AsociatiaFooter") ?>
                                </p>
                                <p>
                                    <a href="http://nixap.com">
                                        <?= Yii::t("app", "DevelopedBy") ?>
                                    </a>
                                </p>
                            </div>
                            <div class="footer-menu uk-text-center">
                                <?= MenuWidget::widget([
                                    'menuId' => 3,
                                ]) ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="mobile_menu" class="uk-offcanvas">
            <div class="uk-offcanvas-bar">
                <a href="/">
                    <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                </a>                
                
                <?= MenuWidget::widget([
                    'menuId' => 3,
                ]) ?> 
                
                <ul class="last-menu">
                    <li>
                        <a href="#">
                            <i class="uk-icon-envelope"></i>
                            <?= Yii::t("app", "Email") ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="uk-icon-sign-in"></i>
                            <?= Yii::t("app", "ClientOnline") ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <?= Yii::t("app", "Forum") ?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="padding1">
                            <?= Html::img($bundle->baseUrl . '/images/capital.png') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <?= Html::img($bundle->baseUrl . '/images/finans-corporation.png') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="margin1">
                            <i><?= Html::img($bundle->baseUrl . '/images/sigla.png') ?>
                            <?= Yii::t("app", "C.N.P.F.") ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        

        
<?php $this->endBody() ?>
<?php $this->registerJsFile("https://maps.googleapis.com/maps/api/js?key=AIzaSyCzj1nfe88iJps7q6Zbw6NsY1qeUtGhzxY&callback=initMap", ['async'=>'' , 'defer'=>'']) ?>
</body>
</html>
<?php $this->endPage() ?>

