<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\AEIFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use app\models\Article\Article;
use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;

$bundle = AEIFrontAssets::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>', THEME_URL = '<?= $bundle->baseUrl ?>';</script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?> 

<div class="wrapper">
    <div class="contact-bg-header">
        <div class="uk-clearfix header-contact-page">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-1-10">
                        <div class="mobile-menu">
                            <a href="#mobile_menu" data-uk-offcanvas>
                                <i class="uk-icon-navicon"></i>
                            </a>
                        </div>
                        <div class="logo">
                            <a href="<?= Url::to(['/']) ?>">
                                <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                            </a>
                        </div>
                    </div>
                    <div class="uk-width-medium-8-10">
                        <div class="menu-nav">
                            <?= MenuWidget::widget([
                                'menuId' => 3,
                            ]) ?>                            
                        </div>
                    </div>
                    <div class="uk-width-medium-1-10">
                        <div class="language-box">
                            <div class="language-box">
                                <?php foreach (Yii::$app->params['siteLanguages'] as $lang) { ?>
                                <?php if(Yii::$app->language == $lang) continue; ?>
                                <a href="<?= Url::current(['language' => $lang]) ?>">
                                    <?= $lang ?>
                                </a>
                                <?php } ?>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="header-title">
                            <h3>
                                <?= $this->title ?>
                            </h3>
                            <h5>
                                <?= Yii::t("app", "Home") ?> - <?= $this->title ?>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="uk-clearfix our-adress">
            <div class="uk-grid">
                <div class="uk-width-medium-6-10">
                    <div class="title-section box1 uk-clearfix">
                        <h3>
                            <?= Yii::t("app", "ContactTitle") ?>
                        </h3>
                    </div>
                    <div id="map" class="item3 contact-page">

                    </div>
                        
                        <?= FeedbackWidget::widget() ?>
                        
                    </div>
                    <div class="uk-width-medium-4-10">
                        <div class="title-section box2">
                            <h3>
                                <?= Yii::t("app", "OurContactsTitle") ?>
                            </h3>
                        </div>
                        <div class="contacts">
                            <div class="contacts-box item3 noMatch">
                                <h4>
                                    <?= Yii::t("app", "Address") ?>
                                </h4>
                                <ul>
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/location.png') ?></i>
                                        <?= Settings::getByName('address', true) ?>
                                    </li>
                                    <li class="padding">
                                        <?= Settings::getByName('address2', true) ?>
                                    </li>
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/phone.png') ?></i>
                                        Tel/fax: <?= Settings::getByName('phone') ?>
                                    </li>
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/mail.png') ?></i>
                                        Email: <?= Settings::getByName('email') ?>
                                    </li>
                                </ul>
                                <h4>
                                    <?= Yii::t("app", "BankInfo") ?>
                                </h4>
                                <ul class="all-padding">
                                    <li>
                                        <i><?= Html::img($bundle->baseUrl . '/images/bank.png') ?></i>
                                        <?= Settings::getByName('bank_rec', true) ?>
                                    </li>
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont1', true) ?>
                                    </li>                                    
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont2', true) ?>
                                    </li>                                    
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont3', true) ?>
                                    </li>
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont4', true) ?>
                                    </li>
                                    <li>
                                        <i class="uk-icon-chevron-right"></i>
                                        <?= Settings::getByName('cont5', true) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-clearfix footer">
                <div class="uk-container-center uk-container">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="copyright uk-text-center">
                                <p>
                                    Copyright &copy; <?= date("Y") ?> <?= Yii::t("app", "AsociatiaFooter") ?>
                                </p>
                                <p>
                                    <a href="http://nixap.com">
                                        <?= Yii::t("app", "DevelopedBy") ?>
                                    </a>
                                </p>
                            </div>
                            <div class="footer-menu uk-text-center">
                                <?= MenuWidget::widget([
                                        'menuId' => 3,
                                    ]) ?>                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="mobile_menu" class="uk-offcanvas">
            <div class="uk-offcanvas-bar">
                <a href="/">
                    <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                </a>                
                
                <?= MenuWidget::widget([
                    'menuId' => 3,
                ]) ?> 
                
                <ul class="last-menu">
                    <li>
                        <a href="#">
                            <i class="uk-icon-envelope"></i>
                            <?= Yii::t("app", "Email") ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="uk-icon-sign-in"></i>
                            <?= Yii::t("app", "ClientOnline") ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <?= Yii::t("app", "Forum") ?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="padding1">
                            <?= Html::img($bundle->baseUrl . '/images/capital.png') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <?= Html::img($bundle->baseUrl . '/images/finans-corporation.png') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="margin1">
                            <i><?= Html::img($bundle->baseUrl . '/images/sigla.png') ?>
                            <?= Yii::t("app", "C.N.P.F.") ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
             
<?php $this->endBody() ?>
<?php $this->registerJsFile("https://maps.googleapis.com/maps/api/js?key=AIzaSyCzj1nfe88iJps7q6Zbw6NsY1qeUtGhzxY&callback=initMap", ['async'=>'' , 'defer'=>'']) ?>
</body>
</html>
<?php $this->endPage() ?>
