-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.48 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table aei.Article
CREATE TABLE IF NOT EXISTS `Article` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `Type` varchar(20) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `Template` varchar(255) DEFAULT NULL,
  `NotRemovable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Article: ~19 rows (approximately)
/*!40000 ALTER TABLE `Article` DISABLE KEYS */;
INSERT INTO `Article` (`ID`, `Date`, `Type`, `Status`, `Template`, `NotRemovable`) VALUES
	(8, '2017-03-16 00:00:00', 'Page', 'Active', NULL, 1),
	(11, '2017-03-16 00:00:00', 'Event', 'Active', NULL, 1),
	(24, '2017-03-30 00:00:00', 'Page', 'Active', 'events', 1),
	(25, '2017-03-30 00:00:00', 'Event', 'Active', NULL, 1),
	(34, '2017-04-04 00:00:00', 'Page', 'Active', 'contacts', 1),
	(35, '2017-03-30 00:00:00', 'Page', 'Active', 'maintenance', 1),
	(84, '2017-04-10 00:00:00', 'Page', 'Active', 'gallery', 1),
	(85, '2017-04-12 00:00:00', 'Page', 'Active', 'about-us', 1),
	(86, '2017-04-12 00:00:00', 'Page', 'Active', 'services-list', 1),
	(87, '2017-04-12 00:00:00', 'Page', 'Active', NULL, 0),
	(88, '2017-04-12 00:00:00', 'Page', 'Active', 'partners', 1),
	(89, '2017-04-12 00:00:00', 'Page', 'Active', 'creditare', 1),
	(90, '2017-04-11 00:00:00', 'Event', 'Active', NULL, 0),
	(91, '2017-04-12 00:00:00', 'Event', 'Active', NULL, 0),
	(92, '2017-04-12 00:00:00', 'Page', 'Active', NULL, 0),
	(93, '2017-04-12 00:00:00', 'Page', 'Active', NULL, 0),
	(94, '2017-04-13 00:00:00', 'Services', 'Active', NULL, 0),
	(95, '2017-04-13 00:00:00', 'Services', 'Active', NULL, 0),
	(96, '2017-04-13 00:00:00', 'Services', 'Active', NULL, 0);
/*!40000 ALTER TABLE `Article` ENABLE KEYS */;


-- Dumping structure for table aei.ArticleFile
CREATE TABLE IF NOT EXISTS `ArticleFile` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleID` int(11) NOT NULL,
  `File` varchar(255) NOT NULL,
  `Type` varchar(255) NOT NULL DEFAULT 'Default',
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FK_ArticleFile_Article` (`ArticleID`),
  CONSTRAINT `FK_ArticleFile_Article` FOREIGN KEY (`ArticleID`) REFERENCES `Article` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.ArticleFile: ~5 rows (approximately)
/*!40000 ALTER TABLE `ArticleFile` DISABLE KEYS */;
INSERT INTO `ArticleFile` (`ID`, `ArticleID`, `File`, `Type`, `Date`, `Position`) VALUES
	(1, 93, 'Certificat-inregistrare-asociatia-centrala_OMhz.pdf', 'application/pdf', '2017-04-12 17:48:51', 0),
	(2, 93, 'Licenta-asociatia-centrala_obmU.pdf', 'application/pdf', '2017-04-12 17:49:47', 0),
	(3, 11, 'Certificat-inregistrare-asociatia-centrala_coha.pdf', 'application/pdf', '2017-04-13 19:30:34', 0),
	(4, 11, 'Licenta-asociatia-centrala_GOR9.pdf', 'application/pdf', '2017-04-13 19:32:56', 0),
	(5, 89, 'Certificat-inregistrare-asociatia-centrala_uKKT.pdf', 'application/pdf', '2017-04-14 18:03:20', 0);
/*!40000 ALTER TABLE `ArticleFile` ENABLE KEYS */;


-- Dumping structure for table aei.ArticleFileLang
CREATE TABLE IF NOT EXISTS `ArticleFileLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleFileID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(1000) NOT NULL,
  `Text` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ArticleFileID_LangID` (`ArticleFileID`,`LangID`),
  CONSTRAINT `FK_ArticleFileLang_ArticleFile` FOREIGN KEY (`ArticleFileID`) REFERENCES `ArticleFile` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.ArticleFileLang: ~6 rows (approximately)
/*!40000 ALTER TABLE `ArticleFileLang` DISABLE KEYS */;
INSERT INTO `ArticleFileLang` (`ID`, `ArticleFileID`, `LangID`, `Title`, `Text`) VALUES
	(1, 1, 'ro', 'Certificat inregistrare asociatia centrala OMhz', ''),
	(2, 1, 'en', 'Certificat inregistrare asociatia centrala OMhz', ''),
	(3, 2, 'ro', 'Licenta asociatia centrala obmU', ''),
	(4, 2, 'en', 'Licenta asociatia centrala obmU', ''),
	(5, 3, 'ro', 'Certificat inregistrare asociatia centrala coha', ''),
	(6, 3, 'en', 'Certificat inregistrare asociatia centrala coha', '');
/*!40000 ALTER TABLE `ArticleFileLang` ENABLE KEYS */;


-- Dumping structure for table aei.ArticleImage
CREATE TABLE IF NOT EXISTS `ArticleImage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleID` int(11) NOT NULL,
  `Thumb` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Position` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ArticleImage_Article` (`ArticleID`),
  CONSTRAINT `FK_ArticleImage_Article` FOREIGN KEY (`ArticleID`) REFERENCES `Article` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.ArticleImage: ~14 rows (approximately)
/*!40000 ALTER TABLE `ArticleImage` DISABLE KEYS */;
INSERT INTO `ArticleImage` (`ID`, `ArticleID`, `Thumb`, `Image`, `Position`) VALUES
	(74, 90, 'thumb_ba1ba667a483b85d713168141d1bc005.png', 'ba1ba667a483b85d713168141d1bc005.png', 0),
	(75, 88, 'thumb_deabd18b7b4c2cd58625c3cf4fbd5365.png', 'deabd18b7b4c2cd58625c3cf4fbd5365.png', 9),
	(76, 88, 'thumb_745f83a69416424efd59c1eb635727ab.png', '745f83a69416424efd59c1eb635727ab.png', 5),
	(77, 88, 'thumb_afe3bc19803c2aa673855a0c67b1eb22.png', 'afe3bc19803c2aa673855a0c67b1eb22.png', 3),
	(78, 88, 'thumb_bd18bd4afe61d12d0dd90e6dc5812da9.png', 'bd18bd4afe61d12d0dd90e6dc5812da9.png', 7),
	(79, 88, 'thumb_781004fe9b2eacedb62abccaf67cbe8e.png', '781004fe9b2eacedb62abccaf67cbe8e.png', 1),
	(80, 88, 'thumb_d274bac58892d90d2633073dcc649c2e.png', 'd274bac58892d90d2633073dcc649c2e.png', 2),
	(81, 88, 'thumb_13b0703f1e63717212f89736ee8e8ebe.png', '13b0703f1e63717212f89736ee8e8ebe.png', 0),
	(82, 88, 'thumb_12fccc2adddeb1c76ec656bee0acfe31.png', '12fccc2adddeb1c76ec656bee0acfe31.png', 8),
	(83, 88, 'thumb_5eef8c835986631919c41399707582ba.png', '5eef8c835986631919c41399707582ba.png', 4),
	(84, 88, 'thumb_f9718e651286c5cd3c5f94e9aca062f9.png', 'f9718e651286c5cd3c5f94e9aca062f9.png', 6),
	(85, 11, 'thumb_93850c71f025cf7a890d6f89f1bc2928.png', '93850c71f025cf7a890d6f89f1bc2928.png', 0),
	(86, 11, 'thumb_9690afbe7c36fe6356e5f7773b77a5ab.png', '9690afbe7c36fe6356e5f7773b77a5ab.png', 0),
	(87, 11, 'thumb_89a031a0c9c36d57dcfd0bb60f2999fa.png', '89a031a0c9c36d57dcfd0bb60f2999fa.png', 0);
/*!40000 ALTER TABLE `ArticleImage` ENABLE KEYS */;


-- Dumping structure for table aei.ArticleImageLang
CREATE TABLE IF NOT EXISTS `ArticleImageLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleImageID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ArticleImageID_LangID` (`ArticleImageID`,`LangID`),
  CONSTRAINT `FK_ArticleImageLang_ArticleImage` FOREIGN KEY (`ArticleImageID`) REFERENCES `ArticleImage` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.ArticleImageLang: ~20 rows (approximately)
/*!40000 ALTER TABLE `ArticleImageLang` DISABLE KEYS */;
INSERT INTO `ArticleImageLang` (`ID`, `ArticleImageID`, `LangID`, `Title`) VALUES
	(1, 78, 'ro', 'http://www.grawe.md/ro/index.htm'),
	(2, 78, 'en', 'http://www.grawe.md/ro/index.htm'),
	(3, 75, 'ro', 'https://www.gardarikacu.ru/'),
	(4, 75, 'en', 'https://www.gardarikacu.ru/'),
	(5, 76, 'ro', 'http://www.capmu.md/'),
	(6, 76, 'en', 'http://www.capmu.md/'),
	(7, 77, 'ro', 'http://www.frankfurt-school.de/home'),
	(8, 77, 'en', 'http://www.frankfurt-school.de/home'),
	(9, 79, 'ro', 'http://www.ifad.md/'),
	(10, 79, 'en', 'http://www.ifad.md/'),
	(11, 80, 'ro', 'https://www.kfw.de/kfw.de.html'),
	(12, 80, 'en', 'https://www.kfw.de/kfw.de.html'),
	(13, 81, 'ro', 'http://www.rfc.md/'),
	(14, 81, 'en', 'http://www.rfc.md/'),
	(15, 82, 'ro', 'https://www.creditunions.ru/folder26/'),
	(16, 82, 'en', 'https://www.creditunions.ru/folder26/'),
	(17, 83, 'ro', 'http://www.woccu.org/'),
	(18, 83, 'en', 'http://www.woccu.org/'),
	(19, 84, 'ro', 'https://nwcua.org/'),
	(20, 84, 'en', 'https://nwcua.org/');
/*!40000 ALTER TABLE `ArticleImageLang` ENABLE KEYS */;


-- Dumping structure for table aei.ArticleLang
CREATE TABLE IF NOT EXISTS `ArticleLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Text` longtext NOT NULL,
  `SeoTitle` varchar(255) NOT NULL,
  `Keywords` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ArticleID_LangID` (`ArticleID`,`LangID`),
  CONSTRAINT `FK_ArticleLang_Article` FOREIGN KEY (`ArticleID`) REFERENCES `Article` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.ArticleLang: ~39 rows (approximately)
/*!40000 ALTER TABLE `ArticleLang` DISABLE KEYS */;
INSERT INTO `ArticleLang` (`ID`, `ArticleID`, `LangID`, `Title`, `Text`, `SeoTitle`, `Keywords`, `Description`) VALUES
	(15, 8, 'ro', 'Principala', '<p>dvf</p>', '', '', ''),
	(16, 8, 'en', 'Home', '<p>Home</p>', '', '', ''),
	(21, 11, 'ro', 'event 1', '<div class="paragraph mt20">\r\n<p>Data de 2 octombrie 2016, va răm&acirc;ne &icirc;n istoria AE&Icirc; &rsquo;Supercredit&rsquo; prin deschiderea oficială a Filialei nr.1 din satul Recea, raionul R&icirc;șcani. AE&Icirc; &rsquo;Supercredit&rsquo;, asociație ce de 15 ani se află pe arena pieții financiare nebancare, a făcut față at&acirc;t crizelor economice ce au fost zdravăn simțite &icirc;n țara noastră , c&acirc;t și schimbărilor legislative, politice, administrative ce au fost de-a lungul anilor. Și actualmente &icirc;n pofida situații instabile din țară, sub conducerea Doamnei Eugenia Pleșca, s-au găsit puteri și resurse pentru deschiderea primei filiale la o distanță de 12 km de orașul R&icirc;șcani &icirc;n satul Recea. La inaugurarea filialei au fost prezenți reprezentanți ai Asociației Centrale a AE&Icirc;, primarul satului Recea, directori și contabili a altor asociații, membri a asociației și locuitorii din satul Recea. &Icirc;n discursurile oaspeților la deschidere s-au regăsit doar cuvinte de laudă și admirație pentru conducerea asociației, apoi a urmat un recital de poezii și c&acirc;ntece a copiilor de la grădinița din localitate, după care Părintele din sat a sfințit și binecuv&acirc;ntat sediul.</p>\r\n<p>Asociaţia Centrală a AE&Icirc; susţine intenţia asociațiilor membre care intenţionează să-şi lărgească aria de activitate, să aducă serviciile oferite de AE&Icirc; &icirc;n mai multe localităţi din Republica Moldova.</p>\r\n</div>', '', '', ''),
	(22, 11, 'en', 'event 1', '<div class="paragraph mt20">\r\n<p>Data de 2 octombrie 2016, va răm&acirc;ne &icirc;n istoria AE&Icirc; &rsquo;Supercredit&rsquo; prin deschiderea oficială a Filialei nr.1 din satul Recea, raionul R&icirc;șcani. AE&Icirc; &rsquo;Supercredit&rsquo;, asociație ce de 15 ani se află pe arena pieții financiare nebancare, a făcut față at&acirc;t crizelor economice ce au fost zdravăn simțite &icirc;n țara noastră , c&acirc;t și schimbărilor legislative, politice, administrative ce au fost de-a lungul anilor. Și actualmente &icirc;n pofida situații instabile din țară, sub conducerea Doamnei Eugenia Pleșca, s-au găsit puteri și resurse pentru deschiderea primei filiale la o distanță de 12 km de orașul R&icirc;șcani &icirc;n satul Recea. La inaugurarea filialei au fost prezenți reprezentanți ai Asociației Centrale a AE&Icirc;, primarul satului Recea, directori și contabili a altor asociații, membri a asociației și locuitorii din satul Recea. &Icirc;n discursurile oaspeților la deschidere s-au regăsit doar cuvinte de laudă și admirație pentru conducerea asociației, apoi a urmat un recital de poezii și c&acirc;ntece a copiilor de la grădinița din localitate, după care Părintele din sat a sfințit și binecuv&acirc;ntat sediul.</p>\r\n<p>Asociaţia Centrală a AE&Icirc; susţine intenţia asociațiilor membre care intenţionează să-şi lărgească aria de activitate, să aducă serviciile oferite de AE&Icirc; &icirc;n mai multe localităţi din Republica Moldova.</p>\r\n</div>', '', '', ''),
	(47, 24, 'ro', 'Evenimente', '<p>Events</p>', '', '', ''),
	(48, 24, 'en', 'Events', '<p>Events</p>', '', '', ''),
	(49, 25, 'ro', 'event 2', '<p>dfg</p>', '', '', ''),
	(50, 25, 'en', 'event 2', '<p>dfg</p>', '', '', ''),
	(51, 35, 'ro', 'Mentenanta', '<p>ment</p>', '', '', ''),
	(52, 35, 'en', 'Maintenance', '<p>ment</p>', '', '', ''),
	(67, 34, 'ro', 'Contacte', '', '', '', ''),
	(68, 34, 'en', 'Contacts', '', '', '', ''),
	(69, 8, 'en', 'en!!', '<p>en!!!</p>', '', '', ''),
	(166, 84, 'ro', 'Fotogalerii', '', '', '', ''),
	(167, 84, 'en', 'Fotogalerii', '', '', '', ''),
	(168, 85, 'ro', 'Despre noi', '<p>Despre noiDespre noiDespre noi</p>', '', '', ''),
	(169, 85, 'en', 'About us', '<p>About usAbout usAbout usAbout us</p>', '', '', ''),
	(170, 86, 'ro', 'Servicii', '<p>Asociaţia Centrală a AE&Icirc; este instituția specializată &icirc;n acordarea suportului pentru asociaţiile de economii şi &icirc;mprumut și susţine dezvoltarea acestui sector. Experienţa vastă din acest domeniu care a fost acumulată datorită respectării standardelor de etică profesională cu utilizarea instrumentelor de comunicare eficientă, asigură un suport profesionist &icirc;n serviciile de consultanţă pentru AE&Icirc;. Echipa noastră este dispusă să ofere asistenţă at&icirc;t asociațiilor membri ai Asociaţiei Centrale şi organelor de conducere a lor, c&icirc;t şi asociațiilor nemembri. Principalele domenii de asistență includ următoarele:</p>', '', '', ''),
	(171, 86, 'en', 'Services', '<p>Asociaţia Centrală a AE&Icirc; este instituția specializată &icirc;n acordarea suportului pentru asociaţiile de economii şi &icirc;mprumut și susţine dezvoltarea acestui sector. Experienţa vastă din acest domeniu care a fost acumulată datorită respectării standardelor de etică profesională cu utilizarea instrumentelor de comunicare eficientă, asigură un suport profesionist &icirc;n serviciile de consultanţă pentru AE&Icirc;. Echipa noastră este dispusă să ofere asistenţă at&icirc;t asociațiilor membri ai Asociaţiei Centrale şi organelor de conducere a lor, c&icirc;t şi asociațiilor nemembri. Principalele domenii de asistență includ următoarele:</p>', '', '', ''),
	(172, 87, 'ro', 'Legislație', '<p>&nbsp;Legislație&nbsp;Legislație&nbsp;Legislație</p>', '', '', ''),
	(173, 87, 'en', 'Legislation', '<p>LegislationLegislationLegislation</p>', '', '', ''),
	(174, 88, 'ro', 'Parteneri', '', '', '', ''),
	(175, 88, 'en', 'Partners', '', '', '', ''),
	(176, 89, 'ro', 'Creditare IFAD', '<p>Creditare IFADCreditare IFADCreditare IFAD</p>', '', '', ''),
	(177, 89, 'en', 'Creditare IFAD', '<p>Creditare IFADCreditare IFADCreditare IFAD</p>', '', '', ''),
	(178, 90, 'ro', 'Eveniment 3', '<p>Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3</p>', '', '', ''),
	(179, 90, 'en', 'Eveniment 3', '<p>Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3Eveniment 3</p>', '', '', ''),
	(180, 91, 'ro', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum  dolor sit amet, consectetuer adipiscing elit.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum&nbsp; dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum&nbsp; dolor sit amet, consectetuer adipiscing elit.</p>', '', '', ''),
	(181, 91, 'en', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum  dolor sit amet, consectetuer adipiscing elit.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum&nbsp; dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum&nbsp; dolor sit amet, consectetuer adipiscing elit.</p>\r\n<p>&nbsp;</p>\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum&nbsp; dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum&nbsp; dolor sit amet, consectetuer adipiscing elit.</p>', '', '', ''),
	(182, 92, 'ro', 'Istoric', '<p>Asociaţia Centrală a Asociaţiilor de Economii şi &Icirc;mprumut (ACAE&Icirc;) a fost fondată la 12 martie 2009 de către 49 de membri şi &icirc;nregistrată la 24 aprilie 2009. Scopul pentru care a fost creată Asociaţia Centrală este de a contribui la dezvoltarea durabilă a asociaţiilor de economii şi &icirc;mprumut şi &icirc;mbunătăţirea calităţii serviciilor acordate de către acestea membrilor lor. Asociaţia Centrala &icirc;şi desfăşoară activitatea &icirc;n conformitate cu Legea asociaţiilor de economii şi &icirc;mprumut (AE&Icirc;), regulamente aferente, precum şi actele normative din domeniu. Fondatorii Asociaţiei Centrale sunt asociaţiile de economii şi &icirc;mprumut care au răspuns la iniţiativa grupului format de AE&Icirc; &icirc;n acest scop. Din numărul total de 124 AE&Icirc; membri, 50 sunt AE&Icirc; cu licenţa de tip B şi 74 AE&Icirc; cu licenţa de tip A</p>', '', '', ''),
	(183, 92, 'en', 'History', '<p>Asociaţia Centrală a Asociaţiilor de Economii şi &Icirc;mprumut (ACAE&Icirc;) a fost fondată la 12 martie 2009 de către 49 de membri şi &icirc;nregistrată la 24 aprilie 2009. Scopul pentru care a fost creată Asociaţia Centrală este de a contribui la dezvoltarea durabilă a asociaţiilor de economii şi &icirc;mprumut şi &icirc;mbunătăţirea calităţii serviciilor acordate de către acestea membrilor lor. Asociaţia Centrala &icirc;şi desfăşoară activitatea &icirc;n conformitate cu Legea asociaţiilor de economii şi &icirc;mprumut (AE&Icirc;), regulamente aferente, precum şi actele normative din domeniu. Fondatorii Asociaţiei Centrale sunt asociaţiile de economii şi &icirc;mprumut care au răspuns la iniţiativa grupului format de AE&Icirc; &icirc;n acest scop. Din numărul total de 124 AE&Icirc; membri, 50 sunt AE&Icirc; cu licenţa de tip B şi 74 AE&Icirc; cu licenţa de tip A</p>', '', '', ''),
	(184, 93, 'ro', 'Acte de Constituirea Asociației Centrale', '', '', '', ''),
	(185, 93, 'en', 'Acte de Constituirea Asociației Centrale', '', '', '', ''),
	(186, 94, 'ro', 'Suport în menţinerea evidenţei contabile în AEÎ și raportare către CNPF', '<ul>\r\n<li><strong>NR1</strong></li>\r\n<li>suport &icirc;n obținerea licenței și instalarea programelor computerizate de evidenţă a portofoliului de &icirc;mprumuturi şi depuneri, evidenţă contabilă - EVIDA și versiuni 1C pentru AE&Icirc;;</li>\r\n<li>suport consultativ și instruire pentru administratorii AE&Icirc; privind utilizarea EVIDA (versiunea integrată cu aparatele de casă); suport &icirc;n menţinerea şi actualizarea programului EVIDA;</li>\r\n<li>suport consultativ la identificarea necesităților AE&Icirc; &icirc;n softuri computerizate (general și specializat) &ndash; Windows, MS Office, versiuni soft contabilitate 1C, etc. cu prestarea informației privind furnizorii autorizați de softuri;</li>\r\n<li>suport &icirc;n instalarea programelor, instruire &icirc;n utilizarea programelor, suport &icirc;n menținerea şi modernizarea programelor pe perioada post-instalare şi post garanţie a programelor instalate;</li>\r\n<li>suport &icirc;n crearea, menţinerea şi asigurarea veridicității bazelor de date a AE&Icirc; pe serverul Asociaţiei Centrale cu posibilitatea de administrare de către AE&Icirc; a bazelor de date de la distanță &icirc;n regim on-line;</li>\r\n<li>suport şi instruiri &icirc;n utilizarea programelor computerizate pentru SIM &ndash; Sisteme de Management Informațional care asigură menţinerea evidenței şi generarea rapoartelor pentru managementul AE&Icirc; şi organele de stat de supraveghere şi control;</li>\r\n<li>suport &icirc;n instalarea programelor de securitate;</li>\r\n<li>suport consultativ &icirc;n definirea parametrilor optimali a echipamentului necesar pentru AE&Icirc; bazat pe corelația calitate preț; suport &icirc;n identificarea furnizorilor de echipament calitativ și performant la prețuri rezonabile.</li>\r\n</ul>\r\n<p>Responsabil:Vladislav Tretiacov</p>\r\n<p>Tel: 022 59 54 30, 0 6938 1508, 0674 33 289</p>', '', '', ''),
	(187, 94, 'en', 'Suport în menţinerea evidenţei contabile în AEÎ și raportare către CNPF', '<ul>\r\n<li>suport &icirc;n obținerea licenței și instalarea programelor computerizate de evidenţă a portofoliului de &icirc;mprumuturi şi depuneri, evidenţă contabilă - EVIDA și versiuni 1C pentru AE&Icirc;;</li>\r\n<li>suport consultativ și instruire pentru administratorii AE&Icirc; privind utilizarea EVIDA (versiunea integrată cu aparatele de casă); suport &icirc;n menţinerea şi actualizarea programului EVIDA;</li>\r\n<li>suport consultativ la identificarea necesităților AE&Icirc; &icirc;n softuri computerizate (general și specializat) &ndash; Windows, MS Office, versiuni soft contabilitate 1C, etc. cu prestarea informației privind furnizorii autorizați de softuri;</li>\r\n<li>suport &icirc;n instalarea programelor, instruire &icirc;n utilizarea programelor, suport &icirc;n menținerea şi modernizarea programelor pe perioada post-instalare şi post garanţie a programelor instalate;</li>\r\n<li>suport &icirc;n crearea, menţinerea şi asigurarea veridicității bazelor de date a AE&Icirc; pe serverul Asociaţiei Centrale cu posibilitatea de administrare de către AE&Icirc; a bazelor de date de la distanță &icirc;n regim on-line;</li>\r\n<li>suport şi instruiri &icirc;n utilizarea programelor computerizate pentru SIM &ndash; Sisteme de Management Informațional care asigură menţinerea evidenței şi generarea rapoartelor pentru managementul AE&Icirc; şi organele de stat de supraveghere şi control;</li>\r\n<li>suport &icirc;n instalarea programelor de securitate;</li>\r\n<li>suport consultativ &icirc;n definirea parametrilor optimali a echipamentului necesar pentru AE&Icirc; bazat pe corelația calitate preț; suport &icirc;n identificarea furnizorilor de echipament calitativ și performant la prețuri rezonabile.</li>\r\n</ul>\r\n<p>Responsabil:Vladislav Tretiacov</p>\r\n<p>Tel: 022 59 54 30, 0 6938 1508, 0674 33 289</p>', '', '', ''),
	(188, 95, 'ro', 'Suport juridic pentru AEI', '<ul>\r\n<li>suport &icirc;n obținerea licenței și instalarea programelor computerizate de evidenţă a portofoliului de &icirc;mprumuturi şi depuneri, evidenţă contabilă - EVIDA și versiuni 1C pentru AE&Icirc;;</li>\r\n<li>suport consultativ și instruire pentru administratorii AE&Icirc; privind utilizarea EVIDA (versiunea integrată cu aparatele de casă); suport &icirc;n menţinerea şi actualizarea programului EVIDA;</li>\r\n<li>suport consultativ la identificarea necesităților AE&Icirc; &icirc;n softuri computerizate (general și specializat) &ndash; Windows, MS Office, versiuni soft contabilitate 1C, etc. cu prestarea informației privind furnizorii autorizați de softuri;</li>\r\n<li>suport &icirc;n instalarea programelor, instruire &icirc;n utilizarea programelor, suport &icirc;n menținerea şi modernizarea programelor pe perioada post-instalare şi post garanţie a programelor instalate;</li>\r\n<li>suport &icirc;n crearea, menţinerea şi asigurarea veridicității bazelor de date a AE&Icirc; pe serverul Asociaţiei Centrale cu posibilitatea de administrare de către AE&Icirc; a bazelor de date de la distanță &icirc;n regim on-line;</li>\r\n<li>suport şi instruiri &icirc;n utilizarea programelor computerizate pentru SIM &ndash; Sisteme de Management Informațional care asigură menţinerea evidenței şi generarea rapoartelor pentru managementul AE&Icirc; şi organele de stat de supraveghere şi control;</li>\r\n<li>suport &icirc;n instalarea programelor de securitate;</li>\r\n<li>suport consultativ &icirc;n definirea parametrilor optimali a echipamentului necesar pentru AE&Icirc; bazat pe corelația calitate preț; suport &icirc;n identificarea furnizorilor de echipament calitativ și performant la prețuri rezonabile.</li>\r\n</ul>\r\n<p>Responsabil:Vladislav Tretiacov</p>\r\n<p>Tel: 022 59 54 30, 0 6938 1508, 0674 33 289</p>', '', '', ''),
	(189, 95, 'en', 'Suport juridic pentru AEI', '<ul>\r\n<li>suport &icirc;n obținerea licenței și instalarea programelor computerizate de evidenţă a portofoliului de &icirc;mprumuturi şi depuneri, evidenţă contabilă - EVIDA și versiuni 1C pentru AE&Icirc;;</li>\r\n<li>suport consultativ și instruire pentru administratorii AE&Icirc; privind utilizarea EVIDA (versiunea integrată cu aparatele de casă); suport &icirc;n menţinerea şi actualizarea programului EVIDA;</li>\r\n<li>suport consultativ la identificarea necesităților AE&Icirc; &icirc;n softuri computerizate (general și specializat) &ndash; Windows, MS Office, versiuni soft contabilitate 1C, etc. cu prestarea informației privind furnizorii autorizați de softuri;</li>\r\n<li>suport &icirc;n instalarea programelor, instruire &icirc;n utilizarea programelor, suport &icirc;n menținerea şi modernizarea programelor pe perioada post-instalare şi post garanţie a programelor instalate;</li>\r\n<li>suport &icirc;n crearea, menţinerea şi asigurarea veridicității bazelor de date a AE&Icirc; pe serverul Asociaţiei Centrale cu posibilitatea de administrare de către AE&Icirc; a bazelor de date de la distanță &icirc;n regim on-line;</li>\r\n<li>suport şi instruiri &icirc;n utilizarea programelor computerizate pentru SIM &ndash; Sisteme de Management Informațional care asigură menţinerea evidenței şi generarea rapoartelor pentru managementul AE&Icirc; şi organele de stat de supraveghere şi control;</li>\r\n<li>suport &icirc;n instalarea programelor de securitate;</li>\r\n<li>suport consultativ &icirc;n definirea parametrilor optimali a echipamentului necesar pentru AE&Icirc; bazat pe corelația calitate preț; suport &icirc;n identificarea furnizorilor de echipament calitativ și performant la prețuri rezonabile.</li>\r\n</ul>\r\n<p>Responsabil:Vladislav Tretiacov</p>\r\n<p>Tel: 022 59 54 30, 0 6938 1508, 0674 33 289</p>', '', '', ''),
	(190, 96, 'ro', 'Elaborarea planului de afaceri pentru AEÎ şi acordarea asistenţei informaţionale necesare ', '<ul>\r\n<li>suport &icirc;n obținerea licenței și instalarea programelor computerizate de evidenţă a portofoliului de &icirc;mprumuturi şi depuneri, evidenţă contabilă - EVIDA și versiuni 1C pentru AE&Icirc;;</li>\r\n<li>suport consultativ și instruire pentru administratorii AE&Icirc; privind utilizarea EVIDA (versiunea integrată cu aparatele de casă); suport &icirc;n menţinerea şi actualizarea programului EVIDA;</li>\r\n<li>suport consultativ la identificarea necesităților AE&Icirc; &icirc;n softuri computerizate (general și specializat) &ndash; Windows, MS Office, versiuni soft contabilitate 1C, etc. cu prestarea informației privind furnizorii autorizați de softuri;</li>\r\n<li>suport &icirc;n instalarea programelor, instruire &icirc;n utilizarea programelor, suport &icirc;n menținerea şi modernizarea programelor pe perioada post-instalare şi post garanţie a programelor instalate;</li>\r\n<li>suport &icirc;n crearea, menţinerea şi asigurarea veridicității bazelor de date a AE&Icirc; pe serverul Asociaţiei Centrale cu posibilitatea de administrare de către AE&Icirc; a bazelor de date de la distanță &icirc;n regim on-line;</li>\r\n<li>suport şi instruiri &icirc;n utilizarea programelor computerizate pentru SIM &ndash; Sisteme de Management Informațional care asigură menţinerea evidenței şi generarea rapoartelor pentru managementul AE&Icirc; şi organele de stat de supraveghere şi control;</li>\r\n<li>suport &icirc;n instalarea programelor de securitate;</li>\r\n<li>suport consultativ &icirc;n definirea parametrilor optimali a echipamentului necesar pentru AE&Icirc; bazat pe corelația calitate preț; suport &icirc;n identificarea furnizorilor de echipament calitativ și performant la prețuri rezonabile.</li>\r\n</ul>\r\n<p>Responsabil:Vladislav Tretiacov</p>\r\n<p>Tel: 022 59 54 30, 0 6938 1508, 0674 33 289</p>', '', '', ''),
	(191, 96, 'en', 'Elaborarea planului de afaceri pentru AEÎ şi acordarea asistenţei informaţionale necesare ', '<ul>\r\n<li>suport &icirc;n obținerea licenței și instalarea programelor computerizate de evidenţă a portofoliului de &icirc;mprumuturi şi depuneri, evidenţă contabilă - EVIDA și versiuni 1C pentru AE&Icirc;;</li>\r\n<li>suport consultativ și instruire pentru administratorii AE&Icirc; privind utilizarea EVIDA (versiunea integrată cu aparatele de casă); suport &icirc;n menţinerea şi actualizarea programului EVIDA;</li>\r\n<li>suport consultativ la identificarea necesităților AE&Icirc; &icirc;n softuri computerizate (general și specializat) &ndash; Windows, MS Office, versiuni soft contabilitate 1C, etc. cu prestarea informației privind furnizorii autorizați de softuri;</li>\r\n<li>suport &icirc;n instalarea programelor, instruire &icirc;n utilizarea programelor, suport &icirc;n menținerea şi modernizarea programelor pe perioada post-instalare şi post garanţie a programelor instalate;</li>\r\n<li>suport &icirc;n crearea, menţinerea şi asigurarea veridicității bazelor de date a AE&Icirc; pe serverul Asociaţiei Centrale cu posibilitatea de administrare de către AE&Icirc; a bazelor de date de la distanță &icirc;n regim on-line;</li>\r\n<li>suport şi instruiri &icirc;n utilizarea programelor computerizate pentru SIM &ndash; Sisteme de Management Informațional care asigură menţinerea evidenței şi generarea rapoartelor pentru managementul AE&Icirc; şi organele de stat de supraveghere şi control;</li>\r\n<li>suport &icirc;n instalarea programelor de securitate;</li>\r\n<li>suport consultativ &icirc;n definirea parametrilor optimali a echipamentului necesar pentru AE&Icirc; bazat pe corelația calitate preț; suport &icirc;n identificarea furnizorilor de echipament calitativ și performant la prețuri rezonabile.</li>\r\n</ul>\r\n<p>Responsabil:Vladislav Tretiacov</p>\r\n<p>Tel: 022 59 54 30, 0 6938 1508, 0674 33 289</p>', '', '', '');
/*!40000 ALTER TABLE `ArticleLang` ENABLE KEYS */;


-- Dumping structure for table aei.ArticleRelation
CREATE TABLE IF NOT EXISTS `ArticleRelation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ParentArticleID` int(11) NOT NULL,
  `ChildArticleID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ParentArticleID_ChildArticleID` (`ParentArticleID`,`ChildArticleID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.ArticleRelation: ~4 rows (approximately)
/*!40000 ALTER TABLE `ArticleRelation` DISABLE KEYS */;
INSERT INTO `ArticleRelation` (`ID`, `ParentArticleID`, `ChildArticleID`) VALUES
	(14, 8, 12),
	(21, 8, 13),
	(22, 85, 92),
	(23, 85, 93);
/*!40000 ALTER TABLE `ArticleRelation` ENABLE KEYS */;


-- Dumping structure for table aei.Feedback
CREATE TABLE IF NOT EXISTS `Feedback` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Feedback: ~10 rows (approximately)
/*!40000 ALTER TABLE `Feedback` DISABLE KEYS */;
INSERT INTO `Feedback` (`ID`, `Name`, `Phone`, `Email`, `Message`, `Date`) VALUES
	(1, 'Maxim Colesnic', '067311180', 'colesnic89@gmail.com', 'Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.', '2017-04-05 12:45:57'),
	(2, 'Test Feedbacker', '056321058', 'test-feedback@mail.ru', 'Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.', '2017-04-05 12:52:06'),
	(3, 'nume prenume', '+333333333', 'ka64yk@yahoo.com', 'test message', '2017-04-18 17:45:50'),
	(4, 'nume prenume', '+333333333', 'ka64yk@yahoo.com', 'test message', '2017-04-18 17:45:51'),
	(5, 'askld askdj', 'hjhgjhgjhgjh', 'asdasda@sdfsdf.com', 'dsdsdsdsdsdsd', '2017-04-18 17:47:36'),
	(6, 'askld askdj', 'hjhgjhgjhgjh', 'asdasda@sdfsdf.com', 'dsdsdsdsdsdsd', '2017-04-18 17:47:37'),
	(7, '11111111111', '22222222222', '33333333@sdfsdf.com', '44444444', '2017-04-18 17:48:53'),
	(8, '11111111111', '22222222222', '33333333@sdfsdf.com', '44444444', '2017-04-18 17:48:54'),
	(9, 'sdfsdf', 'sdfsdf', 'ka64yk@yahoo.com', 'sdfsdfsdf', '2017-04-19 14:26:51'),
	(10, 'sdfsdf', 'sdfsdf', 'ka64yk@yahoo.com', 'sdfsdfsdf', '2017-04-19 14:26:52');
/*!40000 ALTER TABLE `Feedback` ENABLE KEYS */;


-- Dumping structure for table aei.Gallery
CREATE TABLE IF NOT EXISTS `Gallery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `Position` int(11) NOT NULL DEFAULT '0',
  `Type` varchar(50) NOT NULL DEFAULT 'Default',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Gallery: ~2 rows (approximately)
/*!40000 ALTER TABLE `Gallery` DISABLE KEYS */;
INSERT INTO `Gallery` (`ID`, `ParentID`, `Position`, `Type`) VALUES
	(6, 0, 0, 'Default'),
	(7, 0, 0, 'Default');
/*!40000 ALTER TABLE `Gallery` ENABLE KEYS */;


-- Dumping structure for table aei.GalleryAlbum
CREATE TABLE IF NOT EXISTS `GalleryAlbum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Position` int(11) NOT NULL DEFAULT '0',
  `Image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table aei.GalleryAlbum: ~0 rows (approximately)
/*!40000 ALTER TABLE `GalleryAlbum` DISABLE KEYS */;
/*!40000 ALTER TABLE `GalleryAlbum` ENABLE KEYS */;


-- Dumping structure for table aei.GalleryAlbumLang
CREATE TABLE IF NOT EXISTS `GalleryAlbumLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryAlbumID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Text` text,
  PRIMARY KEY (`ID`),
  KEY `GalleryAlbumID_LangID` (`GalleryAlbumID`,`LangID`),
  CONSTRAINT `FK_GalleryAlbumLang_GalleryAlbum` FOREIGN KEY (`GalleryAlbumID`) REFERENCES `GalleryAlbum` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table aei.GalleryAlbumLang: ~0 rows (approximately)
/*!40000 ALTER TABLE `GalleryAlbumLang` DISABLE KEYS */;
/*!40000 ALTER TABLE `GalleryAlbumLang` ENABLE KEYS */;


-- Dumping structure for table aei.GalleryItem
CREATE TABLE IF NOT EXISTS `GalleryItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryID` int(11) NOT NULL,
  `Thumb` text NOT NULL,
  `Value` text NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GalleryID` (`GalleryID`),
  CONSTRAINT `FK_GalleryItem_Gallery` FOREIGN KEY (`GalleryID`) REFERENCES `Gallery` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.GalleryItem: ~7 rows (approximately)
/*!40000 ALTER TABLE `GalleryItem` DISABLE KEYS */;
INSERT INTO `GalleryItem` (`ID`, `GalleryID`, `Thumb`, `Value`, `Type`, `Position`) VALUES
	(62, 6, 'thumb_c9d04fcd52e4bc2c892e7e58a64334b0.png', 'c9d04fcd52e4bc2c892e7e58a64334b0.png', 'Image', 1),
	(63, 6, 'thumb_c23fef87acda950a6a9ddc5175dd56a0.png', 'c23fef87acda950a6a9ddc5175dd56a0.png', 'Image', 2),
	(64, 6, 'thumb_b8fec436e5f10618a6a8c9b1e659ac69.png', 'b8fec436e5f10618a6a8c9b1e659ac69.png', 'Image', 3),
	(65, 7, 'thumb_76ad5bfff56b5cc65c0a54635bb4213f.png', '76ad5bfff56b5cc65c0a54635bb4213f.png', 'Image', 1),
	(66, 7, 'thumb_072f26f0a7e8e1a673e8c87dc0e98f15.png', '072f26f0a7e8e1a673e8c87dc0e98f15.png', 'Image', 2),
	(67, 7, 'thumb_81f6555794d2e06ca4590d32c4717493.png', '81f6555794d2e06ca4590d32c4717493.png', 'Image', 3),
	(68, 7, 'thumb_e56b373c5e7afa45c65614b0c94afff2.png', 'e56b373c5e7afa45c65614b0c94afff2.png', 'Image', 4);
/*!40000 ALTER TABLE `GalleryItem` ENABLE KEYS */;


-- Dumping structure for table aei.GalleryItemLang
CREATE TABLE IF NOT EXISTS `GalleryItemLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryItemID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Text` text,
  PRIMARY KEY (`ID`),
  KEY `GalleryItemID_LangID` (`GalleryItemID`,`LangID`),
  CONSTRAINT `FK_GalleryItemLang_GalleryItem` FOREIGN KEY (`GalleryItemID`) REFERENCES `GalleryItem` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table aei.GalleryItemLang: ~0 rows (approximately)
/*!40000 ALTER TABLE `GalleryItemLang` DISABLE KEYS */;
/*!40000 ALTER TABLE `GalleryItemLang` ENABLE KEYS */;


-- Dumping structure for table aei.GalleryLang
CREATE TABLE IF NOT EXISTS `GalleryLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Text` text,
  PRIMARY KEY (`ID`),
  KEY `GalleryID_LangID` (`GalleryID`,`LangID`),
  CONSTRAINT `FK_GalleryLang_Gallery` FOREIGN KEY (`GalleryID`) REFERENCES `Gallery` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.GalleryLang: ~4 rows (approximately)
/*!40000 ALTER TABLE `GalleryLang` DISABLE KEYS */;
INSERT INTO `GalleryLang` (`ID`, `GalleryID`, `LangID`, `Title`, `Text`) VALUES
	(11, 6, 'ro', 'Galerie Foto', '<p>Galerie Foto</p>'),
	(12, 6, 'en', 'Galerie Foto', '<p>Galerie Foto</p>'),
	(13, 7, 'ro', 'Adunarea generală anuală a membrilor Asociației Centrale din 25 februarie 2016 ', '<h4>Adunarea generală anuală a membrilor Asociației Centrale din 25 februarie 2016</h4>'),
	(14, 7, 'en', 'Adunarea generală anuală a membrilor Asociației Centrale din 25 februarie 2016 ', '<h4>Adunarea generală anuală a membrilor Asociației Centrale din 25 februarie 2016</h4>');
/*!40000 ALTER TABLE `GalleryLang` ENABLE KEYS */;


-- Dumping structure for table aei.Link
CREATE TABLE IF NOT EXISTS `Link` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleID` int(11) NOT NULL,
  `Link` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Link` (`Link`),
  KEY `FK_Link_Article` (`ArticleID`),
  CONSTRAINT `FK_Link_Article` FOREIGN KEY (`ArticleID`) REFERENCES `Article` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Link: ~19 rows (approximately)
/*!40000 ALTER TABLE `Link` DISABLE KEYS */;
INSERT INTO `Link` (`ID`, `ArticleID`, `Link`) VALUES
	(5, 8, 'en'),
	(8, 11, 'event-1'),
	(21, 24, 'evenimente'),
	(22, 25, 'event-2'),
	(23, 35, 'mentenanta'),
	(31, 34, 'contacts'),
	(80, 84, 'fotogalerii'),
	(81, 85, 'about-us'),
	(82, 86, 'services'),
	(83, 87, 'legislation'),
	(84, 88, 'partners'),
	(85, 89, 'creditare-ifad'),
	(86, 90, 'eveniment-3'),
	(87, 91, 'lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit-lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit'),
	(88, 92, 'history'),
	(89, 93, 'acte-de-constituirea-asociatiei-centrale'),
	(90, 94, 'suport-in-mentinerea-evidentei-contabile-in-aei-si-raportare-catre-cnpf'),
	(91, 95, 'suport-juridic-pentru-aei'),
	(92, 96, 'elaborarea-planului-de-afaceri-pentru-aei-si-acordarea-asistentei-informationale-necesare');
/*!40000 ALTER TABLE `Link` ENABLE KEYS */;


-- Dumping structure for table aei.Menu
CREATE TABLE IF NOT EXISTS `Menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Status` varchar(20) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Menu: ~2 rows (approximately)
/*!40000 ALTER TABLE `Menu` DISABLE KEYS */;
INSERT INTO `Menu` (`ID`, `Status`) VALUES
	(3, 'Active'),
	(4, 'Active');
/*!40000 ALTER TABLE `Menu` ENABLE KEYS */;


-- Dumping structure for table aei.MenuItem
CREATE TABLE IF NOT EXISTS `MenuItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuID` int(11) NOT NULL,
  `ArtcileID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `Position` int(11) NOT NULL DEFAULT '999999',
  PRIMARY KEY (`ID`),
  KEY `FK_MenuItem_Menu` (`MenuID`),
  KEY `FK_MenuItem_Article` (`ArtcileID`),
  CONSTRAINT `FK_MenuItem_Article` FOREIGN KEY (`ArtcileID`) REFERENCES `Article` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MenuItem_Menu` FOREIGN KEY (`MenuID`) REFERENCES `Menu` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.MenuItem: ~10 rows (approximately)
/*!40000 ALTER TABLE `MenuItem` DISABLE KEYS */;
INSERT INTO `MenuItem` (`ID`, `MenuID`, `ArtcileID`, `ParentID`, `Position`) VALUES
	(147, 3, 34, 0, 9),
	(164, 3, 84, 133, 12),
	(165, 3, 24, 0, 2),
	(166, 3, 84, 0, 4),
	(167, 3, 85, 0, 3),
	(168, 3, 86, 0, 5),
	(169, 3, 87, 0, 6),
	(170, 3, 88, 0, 7),
	(171, 3, 89, 0, 8),
	(172, 4, 8, 0, 999999);
/*!40000 ALTER TABLE `MenuItem` ENABLE KEYS */;


-- Dumping structure for table aei.MenuItemLang
CREATE TABLE IF NOT EXISTS `MenuItemLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuItemID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MenuItemID_LangID` (`MenuItemID`,`LangID`),
  CONSTRAINT `FK_MenuItemLang_MenuItem` FOREIGN KEY (`MenuItemID`) REFERENCES `MenuItem` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.MenuItemLang: ~20 rows (approximately)
/*!40000 ALTER TABLE `MenuItemLang` DISABLE KEYS */;
INSERT INTO `MenuItemLang` (`ID`, `MenuItemID`, `LangID`, `Title`) VALUES
	(424, 164, 'ro', 'Fotogalerii'),
	(425, 164, 'ru', 'Fotogalerii'),
	(426, 165, 'ro', 'Evenimente'),
	(427, 165, 'en', 'Events'),
	(430, 167, 'ro', 'Despre noi'),
	(431, 167, 'en', 'About us'),
	(432, 168, 'ro', 'Servicii'),
	(433, 168, 'en', 'Services'),
	(434, 169, 'ro', 'Legislație'),
	(435, 169, 'en', 'Legislation'),
	(436, 170, 'ro', 'Parteneri'),
	(437, 170, 'en', 'Partners'),
	(438, 171, 'ro', 'Creditare IFAD'),
	(439, 171, 'en', 'Creditare IFAD'),
	(440, 166, 'ro', 'Galerie'),
	(441, 166, 'en', 'Gallery'),
	(442, 172, 'ro', 'Principala'),
	(443, 172, 'en', 'en!!'),
	(444, 147, 'ro', 'Contacte'),
	(445, 147, 'en', 'Contacts');
/*!40000 ALTER TABLE `MenuItemLang` ENABLE KEYS */;


-- Dumping structure for table aei.MenuLang
CREATE TABLE IF NOT EXISTS `MenuLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MenuID_LangID` (`MenuID`,`LangID`),
  CONSTRAINT `FK_MenuLang_Menu` FOREIGN KEY (`MenuID`) REFERENCES `Menu` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.MenuLang: ~6 rows (approximately)
/*!40000 ALTER TABLE `MenuLang` DISABLE KEYS */;
INSERT INTO `MenuLang` (`ID`, `MenuID`, `LangID`, `Name`) VALUES
	(7, 3, 'ro', 'Principal'),
	(8, 3, 'ru', 'Главное'),
	(9, 4, 'ro', 'Secundar'),
	(10, 4, 'ru', 'Informatii publice'),
	(11, 3, 'en', 'Main'),
	(12, 4, 'en', 'Secondary');
/*!40000 ALTER TABLE `MenuLang` ENABLE KEYS */;


-- Dumping structure for table aei.Settings
CREATE TABLE IF NOT EXISTS `Settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Value` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Settings: ~17 rows (approximately)
/*!40000 ALTER TABLE `Settings` DISABLE KEYS */;
INSERT INTO `Settings` (`ID`, `Name`, `Value`) VALUES
	(1, 'defaultLanguage', 'ro'),
	(2, 'articlesPerPage', '3'),
	(3, 'favicon', '6d2dac2f9596352d1c816187e5aa1975.ico'),
	(4, 'logo', 'a6ed70577b277f9e8a888a7dcdc400b0.png'),
	(5, 'mentenance', ''),
	(6, 'phone', '(+373 22) 59 54 30'),
	(8, 'email', 'colesnic89@gmail.com'),
	(9, 'address', 'a:2:{s:2:"ro";s:28:"MD 2069, Republica Moldova\r\n";s:2:"en";s:26:"MD 2069, Republica Moldova";}'),
	(11, 'map', ''),
	(21, 'address2', 'a:2:{s:2:"ro";s:38:"or. Chişinău, str. Ion Creangă 10/5";s:2:"en";s:38:"or. Chişinău, str. Ion Creangă 10/5";}'),
	(22, 'bank_rec', 'a:2:{s:2:"ro";s:34:"A.E.I.ASOCIAȚIA CENTRALĂ a A.E.I";s:2:"en";s:34:"A.E.I.ASOCIAȚIA CENTRALĂ a A.E.I";}'),
	(23, 'cont1', 'a:2:{s:2:"ro";s:117:"CONTUL BANCAR pentru Fondul Depunerilor de <br />Economii (FDE) 22511788646; <br />Cod IBAN MD36AG000000022511788646 ";s:2:"en";s:111:"CONTUL BANCAR pentru Fondul Depunerilor de Economii (FDE) 22511788646; <br />Cod IBAN MD36AG000000022511788646 ";}'),
	(24, 'cont2', 'a:2:{s:2:"ro";s:101:"MD36AG000000022511788646 Instituțională (FRI) 22512041549; <br />Cod IBAN MD67AG000000022512041549 ";s:2:"en";s:101:"MD36AG000000022511788646 Instituțională (FRI) 22512041549; <br />Cod IBAN MD67AG000000022512041549 ";}'),
	(25, 'cont3', 'a:2:{s:2:"ro";s:85:"CONTUL BANCAR pentru cote membru 22511358493;<br />Cod IBAN MD66AG000000022511358493 ";s:2:"en";s:86:"CONTUL BANCAR pentru cote membru 22511358493; <br />Cod IBAN MD66AG000000022511358493 ";}'),
	(26, 'cont4', 'a:2:{s:2:"ro";s:27:" CODUL FISCAL 1009600014775";s:2:"en";s:27:" CODUL FISCAL 1009600014775";}'),
	(27, 'cont5', 'a:2:{s:2:"ro";s:79:"BANCA BC Moldova Agroindbank S.A., or. Chișinău<br />\r\nCODUL BANCII AGRNMD2X ";s:2:"en";s:79:"BANCA BC Moldova Agroindbank S.A., or. Chișinău<br />\r\nCODUL BANCII AGRNMD2X ";}'),
	(28, 'topBarText', 'a:2:{s:2:"ro";s:22:"Site-ul oficial Soroca";s:2:"en";s:0:"";}');
/*!40000 ALTER TABLE `Settings` ENABLE KEYS */;


-- Dumping structure for table aei.Slider
CREATE TABLE IF NOT EXISTS `Slider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.Slider: ~1 rows (approximately)
/*!40000 ALTER TABLE `Slider` DISABLE KEYS */;
INSERT INTO `Slider` (`ID`, `Name`) VALUES
	(1, 'Home slider');
/*!40000 ALTER TABLE `Slider` ENABLE KEYS */;


-- Dumping structure for table aei.SliderItem
CREATE TABLE IF NOT EXISTS `SliderItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SliderID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Position` int(11) NOT NULL DEFAULT '999999',
  PRIMARY KEY (`ID`),
  KEY `FK_SliderItem_Slider` (`SliderID`),
  CONSTRAINT `FK_SliderItem_Slider` FOREIGN KEY (`SliderID`) REFERENCES `Slider` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.SliderItem: ~2 rows (approximately)
/*!40000 ALTER TABLE `SliderItem` DISABLE KEYS */;
INSERT INTO `SliderItem` (`ID`, `SliderID`, `Image`, `Position`) VALUES
	(4, 1, '4b14a1db649dbe779f7452a425a064a2.jpg', 999999),
	(5, 1, 'a8ebd1e861eacb7a1415327e7b5b41f7.jpg', 999999);
/*!40000 ALTER TABLE `SliderItem` ENABLE KEYS */;


-- Dumping structure for table aei.SliderItemLang
CREATE TABLE IF NOT EXISTS `SliderItemLang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SliderItemID` int(11) NOT NULL,
  `LangID` varchar(2) NOT NULL,
  `Title` varchar(255) NOT NULL DEFAULT '',
  `Text` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `SliderItemID_LangID` (`SliderItemID`,`LangID`),
  CONSTRAINT `FK_SliderItemLang_SliderItem` FOREIGN KEY (`SliderItemID`) REFERENCES `SliderItem` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.SliderItemLang: ~4 rows (approximately)
/*!40000 ALTER TABLE `SliderItemLang` DISABLE KEYS */;
INSERT INTO `SliderItemLang` (`ID`, `SliderItemID`, `LangID`, `Title`, `Text`) VALUES
	(7, 4, 'ro', 'Primaria Soroca', 'Primaria Soroca'),
	(8, 4, 'ru', 'Primaria Soroca', 'Primaria Soroca'),
	(9, 5, 'ro', 'Primaria Soroca', 'Primaria Soroca'),
	(10, 5, 'ru', 'Primaria Soroca', 'Primaria Soroca');
/*!40000 ALTER TABLE `SliderItemLang` ENABLE KEYS */;


-- Dumping structure for table aei.User
CREATE TABLE IF NOT EXISTS `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `AuthKey` varchar(50) DEFAULT NULL,
  `Pin` varchar(50) DEFAULT NULL,
  `Name` varchar(255) NOT NULL,
  `Status` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table aei.User: ~3 rows (approximately)
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` (`ID`, `Email`, `Password`, `AuthKey`, `Pin`, `Name`, `Status`) VALUES
	(1, 'mihai@nixap.com', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, 'Admin', 'Active'),
	(3, 'colesnic89@gmail.com', '24557', NULL, NULL, 'Admin', 'Inactive'),
	(4, 'client2@gmail.com', '1111', NULL, NULL, 'Client 2', 'Active');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;


-- Dumping structure for table aei.UserPermission
CREATE TABLE IF NOT EXISTS `UserPermission` (
  `UserID` int(11) NOT NULL,
  `Module` varchar(255) NOT NULL,
  `Permission` varchar(255) NOT NULL,
  KEY `FK_UserPermision_User` (`UserID`),
  CONSTRAINT `FK_UserPermision_User` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table aei.UserPermission: ~9 rows (approximately)
/*!40000 ALTER TABLE `UserPermission` DISABLE KEYS */;
INSERT INTO `UserPermission` (`UserID`, `Module`, `Permission`) VALUES
	(3, 'dashboard', 'view_dashboard'),
	(3, 'user', 'list_users'),
	(1, 'user', 'change_permissions'),
	(1, 'user', 'list_users'),
	(1, 'user', 'delete_user'),
	(1, 'user', 'create_user'),
	(1, 'dashboard', 'view_dashboard'),
	(1, 'project', 'view_projects'),
	(3, 'project', 'view_projects');
/*!40000 ALTER TABLE `UserPermission` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
