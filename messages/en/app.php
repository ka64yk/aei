<?php

return [
    'General settings' => 'General settings',
    'Default language' => 'Default language',
    'Contacts' => 'Contacts',
    
    'AsociatiaCentralaEconomii' => 'ASOCIAȚIA CENTRALĂ A ASOCIAȚIILOR DE <br />ECONOMII ȘI ÎMPRUMUT',
    'Email' => 'Email',
    'ClientOnline' => 'Online Client',
    'Forum' => 'Forum',
    'C.N.P.F.' => 'C.N.P.F.',
    'LatestEvents' => 'LATEST <span>EVENTS</span>',
    'ViewMore' => 'View More',
    'AboutUsTitle' => 'ABOUT <span>US</span>',
    'OurServiceTitle' => 'OUR <span>SERVICES</span>',
    'OurServices' => 'Our Services',
    'PartnersTitle' => 'OUR <span>PARTNERS</span>',
    'ContactTitle' => 'FIND <span>US</span>',
    'OurContactsTitle' => 'OUR <span>CONTACTS</span>',
    'Address' => 'ADDRESS',
    'BankInfo' => 'BANK DETAILS',
    'AsociatiaFooter' => 'Asociația Centrală a Asociațiilor de Economii și Împrumut',
    'DevelopedBy' => 'Developed by Nixap',
    'Home' => 'HOME',
    'Attachments' => 'Attachments',
    'Gallery' => 'Gallery',
    //'' => '',
    
];