<?php

namespace app\models\NewsLang;

use Yii;

/**
 * This is the model class for table "NewsLang".
 *
 * @property integer $ID
 * @property integer $NewsID
 * @property integer $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property News $news
 */
class NewsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'NewsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Title', 'Text'], 'required'],
            [['NewsID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'SeoTitle'], 'string', 'max' => 255],
            [['Keywords', 'Description'], 'string', 'max' => 500],
            [['NewsID'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NewsID' => 'News ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Text' => 'Text',
            'SeoTitle' => 'Seo Title',
            'Keywords' => 'Keywords',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['ID' => 'NewsID']);
    }
}
