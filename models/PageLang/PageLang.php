<?php

namespace app\models\PageLang;

use Yii;

/**
 * This is the model class for table "PageLang".
 *
 * @property integer $ID
 * @property integer $PageID
 * @property integer $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property Page $page
 */
class PageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PageLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title', 'Text'], 'required'],
            [['LangID'], 'string', 'max' => 255],
            [['Text'], 'string'],
            [['Title', 'SeoTitle'], 'string', 'max' => 255],
            [['Keywords', 'Description'], 'string', 'max' => 500],
            [['PageID'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PageID' => 'Page ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Text' => 'Text',
            'SeoTitle' => 'Seo Title',
            'Keywords' => 'Keywords',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['ID' => 'PageID']);
    }
}
