<?php

namespace app\models\Feedback;

use Yii;

/**
 * This is the model class for table "Feedback".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Phone
 * @property string $Email
 * @property string $Message
 * @property string $Date
 */
class Feedback extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Phone', 'Email', 'Message'], 'required'],
            [['Message'], 'string'],
            [['Date'], 'safe'],
            [['Name', 'Phone', 'Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => Yii::t("app", 'Name'),
            'Phone' => Yii::t("app", 'Phone'),
            'Email' => Yii::t("app", 'Email'),
            'Message' => Yii::t("app", 'Message'),
            'Date' => Yii::t("app", 'Date'),
        ];
    }
}
